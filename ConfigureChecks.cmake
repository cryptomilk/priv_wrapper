include(CheckIncludeFile)
include(CheckSymbolExists)
include(CheckFunctionExists)
include(CheckLibraryExists)
include(CheckTypeSize)
include(CheckStructHasMember)
include(CheckPrototypeDefinition)
include(TestBigEndian)

set(PRIV_WRAPPER_PACKAGE ${PROJECT_NAME})
set(PRIV_WRAPPER_VERSION ${PROJECT_VERSION})

set(BINARYDIR ${CMAKE_BINARY_DIR})
set(SOURCEDIR ${CMAKE_SOURCE_DIR})

function(COMPILER_DUMPVERSION _OUTPUT_VERSION)
    # Remove whitespaces from the argument.
    # This is needed for CC="ccache gcc" cmake ..
    string(REPLACE " " "" _C_COMPILER_ARG "${CMAKE_C_COMPILER_ARG1}")

    execute_process(
        COMMAND
            ${CMAKE_C_COMPILER} ${_C_COMPILER_ARG} -dumpversion
        OUTPUT_VARIABLE _COMPILER_VERSION
    )

    string(REGEX REPLACE "([0-9])\\.([0-9])(\\.[0-9])?" "\\1\\2"
           _COMPILER_VERSION "${_COMPILER_VERSION}")

    set(${_OUTPUT_VERSION} ${_COMPILER_VERSION} PARENT_SCOPE)
endfunction()

if(CMAKE_COMPILER_IS_GNUCC AND NOT MINGW AND NOT OS2)
    compiler_dumpversion(GNUCC_VERSION)
    if (NOT GNUCC_VERSION EQUAL 34)
        set(CMAKE_REQUIRED_FLAGS "-fvisibility=hidden")
        check_c_source_compiles(
"void __attribute__((visibility(\"default\"))) test() {}
int main(void){ return 0; }
" WITH_VISIBILITY_HIDDEN)
        unset(CMAKE_REQUIRED_FLAGS)
    endif (NOT GNUCC_VERSION EQUAL 34)
endif(CMAKE_COMPILER_IS_GNUCC AND NOT MINGW AND NOT OS2)

# HEADERS
check_include_file(sys/prctl.h HAVE_SYS_PRCTL_H)
check_include_file(sys/time.h HAVE_SYS_TIME_H)
check_include_file(sys/resource.h HAVE_SYS_RESOURCE_H)

# FUNCTIONS
check_function_exists(chroot HAVE_CHROOT)
check_function_exists(prctl HAVE_PRCTL)
check_function_exists(pledge HAVE_PLEDGE)
check_function_exists(setrlimit HAVE_SETRLIMIT)


if (UNIX)
    find_library(DLFCN_LIBRARY dl)
    if (DLFCN_LIBRARY)
        list(APPEND _REQUIRED_LIBRARIES ${DLFCN_LIBRARY})
    else()
        check_function_exists(dlopen HAVE_DLOPEN)
        if (NOT HAVE_DLOPEN)
            message(FATAL_ERROR "FATAL: No dlopen() function detected")
        endif()
    endif()
endif (UNIX)

###########################################################
# For detecting attributes we need to treat warnings as
# errors
set(CMAKE_REQUIRED_FLAGS "-Werror")

check_c_source_compiles("
void test_constructor_attribute(void) __attribute__ ((constructor));

void test_constructor_attribute(void)
{
    return;
}

int main(void) {
    return 0;
}" HAVE_CONSTRUCTOR_ATTRIBUTE)

check_c_source_compiles("
void test_destructor_attribute(void) __attribute__ ((destructor));

void test_destructor_attribute(void)
{
    return;
}

int main(void) {
    return 0;
}" HAVE_DESTRUCTOR_ATTRIBUTE)

check_c_source_compiles("
#define FALL_THROUGH __attribute__((fallthrough))

int main(void) {
    int i = 2;

    switch (i) {
    case 0:
        FALL_THROUGH;
    case 1:
        break;
    default:
        break;
    }

    return 0;
}" HAVE_FALLTHROUGH_ATTRIBUTE)

check_c_source_compiles("
void log_fn(const char *format, ...) __attribute__ ((format (printf, 1, 2)));

int main(void) {
    return 0;
}" HAVE_FUNCTION_ATTRIBUTE_FORMAT)

check_c_source_compiles("
void test_address_sanitizer_attribute(void) __attribute__((no_sanitize_address));

void test_address_sanitizer_attribute(void)
{
    return;
}

int main(void) {
    return 0;
}" HAVE_ADDRESS_SANITIZER_ATTRIBUTE)

# Stop treating wanrings as errors
unset(CMAKE_REQUIRED_FLAGS)
###########################################################
# ENDIAN
if (NOT WIN32)
    test_big_endian(WORDS_BIGENDIAN)
endif (NOT WIN32)

set(PWRAP_REQUIRED_LIBRARIES ${_REQUIRED_LIBRARIES} CACHE INTERNAL "priv_wrapper required system libraries")
