/* Name of package */
#cmakedefine PRIV_WRAPPER_PACKAGE "${PRIV_WRAPPER_PACKAGE}"

/* Version number of package */
#cmakedefine PRIV_WRAPPER_VERSION "${PRIV_WRAPPER_VERSION}"

#cmakedefine BINARYDIR "${BINARYDIR}"
#cmakedefine SOURCEDIR "${SOURCEDIR}"

/************************** HEADER FILES *************************/

#cmakedefine HAVE_SYS_PRCTL_H 1
#cmakedefine HAVE_SYS_TIME_H 1
#cmakedefine HAVE_SYS_RESOURCE_H 1

/*************************** FUNCTIONS ***************************/

#cmakedefine HAVE_PRCTL 1
#cmakedefine HAVE_SETRLIMIT 1
#cmakedefine HAVE_CHROOT 1
#cmakedefine HAVE_PLEDGE 1

/**************************** OPTIONS ****************************/

#cmakedefine HAVE_CONSTRUCTOR_ATTRIBUTE 1
#cmakedefine HAVE_DESTRUCTOR_ATTRIBUTE 1
#cmakedefine HAVE_FALLTHROUGH_ATTRIBUTE 1
#cmakedefine HAVE_ADDRESS_SANITIZER_ATTRIBUTE 1
#cmakedefine HAVE_FUNCTION_ATTRIBUTE_FORMAT 1

/*************************** ENDIAN *****************************/

/* Define WORDS_BIGENDIAN to 1 if your processor stores words with the most
   significant byte first (like Motorola and SPARC, unlike Intel). */
#cmakedefine WORDS_BIGENDIAN 1
