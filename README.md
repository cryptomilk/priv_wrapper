PRIV_WRAPPER
============

This is a wrapper to disable resource limits and other privilege dropping.

DESCRIPTION
-----------

More details can be found in the manpage:

  man -l ./doc/priv_wrapper.1

or the raw text version:

  less ./doc/priv_wrapper.1.txt

For installation instructions please take a look at the README.install file.

UNIT TESTS
-----------
Steps to run unit tests:

mkdir obj
cd obj
cmake -DUNIT_TESTING=ON ..
make
ctest  # ctest --output-on-failure

MAILINGLIST
-----------

As the mailing list samba-technical is used and can be found here:

https://lists.samba.org/mailman/listinfo/samba-technical
